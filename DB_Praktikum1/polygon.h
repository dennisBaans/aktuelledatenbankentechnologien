/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   polygon.h
 * Author: itachidhdr
 *
 * Created on 7. April 2018, 11:43
 */

#ifndef POLYGON_H
#define POLYGON_H

#include <vector>
#include "point.h"

class Polygon {
    
    
public:
    Polygon();
    Polygon( std::vector<Point> polygonPoints);
    
    std::vector<Point> mur();
    std::vector<Point> konvexHuelle();
    
    void print();

private:
    std::vector<Point> exteriorRing;
    
    int getP0Index(std::vector<Point>);

};

#endif /* POLYGON_H */

