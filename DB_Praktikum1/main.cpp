/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   main.cpp
 * Author: itachidhdr
 *
 * Created on 7. April 2018, 11:35
 */

#include <iostream>
#include <vector>
#include "point.h"
#include "polygon.h"

using namespace std;

/*
 * 
 */
int main(int argc, char** argv) {
    
    vector<Point> polygonPoints;
    polygonPoints.push_back(Point(1,5));
    polygonPoints.push_back(Point(2,2));
    polygonPoints.push_back(Point(2,7));
    polygonPoints.push_back(Point(4,4));
    polygonPoints.push_back(Point(5,13));
    polygonPoints.push_back(Point(6,1));
    polygonPoints.push_back(Point(7,4));
    polygonPoints.push_back(Point(9,6));
    polygonPoints.push_back(Point(10,5));
    
    Polygon polygon (polygonPoints);
    Polygon polygonMur (polygon.mur());
    Polygon polygonCH (polygon.konvexHuelle());
    
    
    cout << "Aktuelle Datenbankentechnologien Praktikum 1" << endl;
    cout << "Minimal Umgebendes Recheck & Konvexe Huelle" << endl << endl;
    cout << "Polygon: ";
    
    polygon.print();
    
    cout << endl << "MUR: ";
    
    polygonMur.print();
    
    cout << endl << endl << "Konvexe Hülle: ";
    
    polygonCH.print();
    
   
    cout << endl << endl;
    return 0;
}

