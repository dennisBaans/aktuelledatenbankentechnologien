/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   polygon.cpp
 * Author: itachidhdr
 * 
 * Created on 7. April 2018, 11:43
 */

#include "polygon.h"
#include <algorithm>   
#include <iostream>

bool sortInt(int i, int j){ return (i<j);}

Polygon::Polygon() {
    Polygon::exteriorRing.push_back(Point(0,0));
    Polygon::exteriorRing.push_back(Point(0,1));
    Polygon::exteriorRing.push_back(Point(1,0));
}

Polygon::Polygon(std::vector<Point> polygonPoints){
    Polygon::exteriorRing = polygonPoints;
    
}

std::vector<Point> Polygon::mur(){
    
    std::vector<Point> murPoints;
    
    std::vector<int> xValues;
    std::vector<int> yValues;
    
    for(Point p : Polygon::exteriorRing){
        xValues.push_back(p.getX());
        yValues.push_back(p.getY());
    }
    
    std::sort (xValues.begin(), xValues.end());
    std::sort (yValues.begin(), yValues.end());
    
    murPoints.push_back(Point(xValues.front(),yValues.back()));
    murPoints.push_back(Point(xValues.front(),yValues.front()));
    murPoints.push_back(Point(xValues.back(),yValues.back()));
    murPoints.push_back(Point(xValues.back(),yValues.front()));
    
    return murPoints;
}

Point p0;


// returns: -1 = rechts; +1 = links; 0 = gleich
int winkelBestimmung(Point a, Point b, Point c) {
    int area = (b.getX() - a.getX()) * (c.getY() - a.getY()) - (b.getY() - a.getY()) * (c.getX() - a.getX());
    if (area > 0)
        return -1;
    else if (area < 0)
        return 1;
    return 0;
}

int sqrDist(Point a, Point b)  {
    int dx = a.getX() - b.getX(), 
        dy = a.getY() - b.getY();
    
    return dx * dx + dy * dy;
}

bool winkelSortierung(Point a, Point b)  {
    int order = winkelBestimmung(p0, a, b);
    if (order == 0)
        return sqrDist(p0, a) < sqrDist(p0, b);
    return (order == -1);
}



std::vector<Point> Polygon::konvexHuelle(){
    
    std::vector<Point> CH;
    
    std::vector<Point> tmpExteriorRing = Polygon::exteriorRing;
    int indexP0 = Polygon::getP0Index(tmpExteriorRing);
    
    //P0 an erste stelle tauschen
    Point tmp = tmpExteriorRing[0];
    tmpExteriorRing[0] = tmpExteriorRing[indexP0];
    tmpExteriorRing[indexP0] = tmp;
    
    p0 = tmpExteriorRing[0];
    
    std::sort (tmpExteriorRing.begin() + 1, tmpExteriorRing.end(), winkelSortierung);
    
    CH.push_back(tmpExteriorRing[0]);
    CH.push_back(tmpExteriorRing[1]);
    CH.push_back(tmpExteriorRing[2]);

    for (int i = 3; i < tmpExteriorRing.size(); i++) {
        Point top = CH.back();
        CH.pop_back();
        while (winkelBestimmung(CH.back(), top, tmpExteriorRing[i]) != -1)   {
            top = CH.back();
            CH.pop_back();
        }
        CH.push_back(top);
        CH.push_back(tmpExteriorRing[i]);
    }
return CH;
    
    
    
    
    
}

void Polygon::print(){
    for(Point point : Polygon::exteriorRing){
        point.print();
    }
}

// Private

int Polygon::getP0Index(std::vector<Point> vPoints){ 
    Point min = vPoints.at(0);
    int index = 0;
    for(int i = 0; i < vPoints.size(); i++){
        Point p = vPoints[i];
        if(min.getY() > p.getY()){
            min = p;
            index = i;
        }else if(min.getY() == p.getY()){
            if(min.getX() > p.getX()){
                min = p;
                index = i;
            }
        }
    }
    
    return index;    
}
