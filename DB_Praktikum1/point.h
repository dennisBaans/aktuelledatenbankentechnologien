/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   point.h
 * Author: itachidhdr
 *
 * Created on 7. April 2018, 11:44
 */



#ifndef POINT_H
#define POINT_H

#include<string>

using std::string;


class Point {
public:
    Point();
    Point(int x, int y);
    
    int getX();
    int getY();
    
    void print();
    
private:
    int x;
    int y;

};

#endif /* POINT_H */

