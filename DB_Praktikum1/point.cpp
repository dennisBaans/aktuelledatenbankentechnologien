/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   point.cpp
 * Author: itachidhdr
 * 
 * Created on 7. April 2018, 11:44
 */

#include "point.h"
#include <iostream>

Point::Point() {
    x = 0;
    y = 0;
}

Point::Point(int x, int y) {
    Point::x = x;
    Point::y = y;
}

int Point::getX(){return x;}
int Point::getY(){return y;}

void Point::print(){

    std::cout << "(" << Point::x << "," << Point::y << ")";
    
}

